import React from 'react';
import { ContentCard } from '../components/Card';
import {  TrimmedInfoGraphicSection, TrimmedImageSection } from '../components/InfoSection';
import { eggObjOne, eggObjTree, eggObjTwo, eggObjFour, eggObjFive } from '../components/InfoSection/Data';
import FooterPage from '../components/FooterPage';



const EggsPage = () => {
  return (
    <>
      <TrimmedInfoGraphicSection {...eggObjOne}/>
      <ContentCard/>
      <TrimmedInfoGraphicSection {...eggObjTwo}/>
      <TrimmedImageSection {...eggObjTree}/>
      <TrimmedInfoGraphicSection {...eggObjFour}/>
      <TrimmedInfoGraphicSection {...eggObjFive}/>
      <FooterPage />
    </>
  );
};

export default EggsPage;