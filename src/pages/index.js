import React, {useState} from 'react';
import HeroSection from '../components/HeroSection';
import { Card } from '../components/Card';
import { InfoSectionBig, InfoSection, InfoGraphicSection, VideoInfoSection } from '../components/InfoSection';
import { homeObjOne, homeObjTwo, homeObjTree, homeObjFour, wildCard } from '../components/InfoSection/Data';
import Services from '../components/Services';
import TeamCard from '../components/TeamCard';
import Footer from '../components/Footer';
const Home = () => {
 

  return (
      <>
      <HeroSection />
      <InfoSectionBig {...homeObjOne}/>
      <Card {...wildCard}/>
      <Services />
      <VideoInfoSection {...homeObjTwo}/>
      <InfoGraphicSection {...homeObjTree}/>
      <TeamCard />
      <Footer />
      </>
  );
};

export default Home;
