import React, {Component, useState} from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import './App.css';
import Home from './pages';
import EggsPage from './pages/info_eggs';
import Sidebar from './components/Sidebar';
import Navbar from './components/Navbar';

function App() 
{ //questa funzione rappresenta un componente react
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  }

  return(
    <Router>
      <Sidebar isOpen={isOpen} toggle={toggle}/>
      <Navbar toggle={toggle}/>
      <Routes>
        <Route path="/" element={<Home/>} exact />
        <Route path="/info_eggs" element={<EggsPage/>} exact />
      </Routes>
      
    </Router>
  );
}

export default App;

//Switch to Routes