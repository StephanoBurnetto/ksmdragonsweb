import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'; //nota quest'import
import 'bootstrap/dist/css/bootstrap.css';
import './font.css';

ReactDOM.render( 
  //questa funtion si occupa di reindirizzare App, il component rappresentante la pagina
  //primo argomento: quale elem reindirizzare secondo:dove
  <React.StrictMode> 
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

