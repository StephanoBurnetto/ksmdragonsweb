import styled from "styled-components";
import { Link as LinkR } from 'react-router-dom';
import { Link as LinkS } from 'react-scroll';
import { TextWrapper } from "../InfoSection/InfoElementsBig";

export const CardContainer = styled.div`
    background: Transparent;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 200px;
    padding: 0px 0px; //top n boot 0 left n right 30
    position: relative;
    z-index: 1;
    bottom: 0px;
    left: 0px;
    transform: none;
    margin-right: 0px;
    
    @media screen and (max-width: 767px)
    {
        margin-top: 100px;
    }  
`;

export const CardContainerC = styled.div`
    position: relative;
    width: 100%;
    height: fit-content;
    display:flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    padding: 30px;
    & .card:hover .text
    {
        visibility: visible;
        opacity: 1;
        margin-top: -40px;
    }
    /*& .card:hover .imgBox h1
    {
        //color: rgb(205, 53, 148);
        top: -65px;
    }
    @media screen and (max-width: 1665px)
    {
        height: fit-content;
    }*/
  
`;

export const CardC = styled.div`
    position: relative;
    max-width: 400px;
    height: 260px;
    background: #000;
    margin: 30px 10px;
    padding: 20px 15px;
    display: flex;
    flex-direction: column;
    box-shadow: 0 5px 20px rgba(0,0,0,0.5);
    transition: 0.3s ease-in-out;
    overflow: visible;
    border-radius: 5px;
    &:hover
    {
        height: 550px;
    }

    @media screen and (max-width: 768px)
    {
        height: auto;
        &:hover
        {
            height: 650px;
        }
    }
`;

export const CardLinkerMini = styled.div`
color: red;
justify-self: flex;
cursor: pointer;
font-size: 1.5rem;
display: flex:
align-items: center;
margin-left: 0px;
font-weight: bold;
text-decoration: none;
transition: all 2s ease-in;
display: none;
@media screen and (max-width: 768px)
  {
      //display: flex;
  }  



`;

export const CardLinker2= styled.div`
  color: red;
  justify-self: flex;
  cursor: pointer;
  font-size: 1.5rem;
  display: flex:
  align-items: center;
  margin-left: 0px;
  font-weight: bold;
  text-decoration: none;
  transition: all 2s ease-in;

    @media screen and (max-width: 420px)
    {
        //margin-left: 80px;
        width: 412px;
    }  

`;


export const CardContent2 = styled.video`
    width:  810px;
    height: 610px;
    margin: 10px 10px 10px 10px;
    position:relative;
    -o-object-fit: cover;
    object-fit: cover;
    right: 50px;

    @media screen and (max-width: 820px)
    {
        left: 0px;
        
    }
    @media screen and (max-width: 768px)
    {
        left: 50px;
        width: 610px;
        right: 0px;
    }
    @media screen and (max-width: 420px)
    {
        margin: 0px 0px 0px 0px;
        right: 0px;
        left: 0px;
        width: 412px;
        
    }

  
`;


export const CardP = styled.div`
    background: transparent;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0px 0px; //top n boot 0 left n right 30

    position: relative;
    z-index: 1;

    @media screen and (max-width: 10240px)
    {
        font-size: 24px;
        display:grid;
        
    }

    @media screen and (max-width: 480px)
    {
        font-size: 18px;
    }   
`;

export const TextWrapperCard = styled(TextWrapper)`
    max-width: 1000px;
    padding-top: 0;
    padding-bottom: 60px;
    
`;

export const TopLine = styled.p`
    color:#01bf71;
    font-size: 32px;
    line height: 16px;
    font-weight: 700;
    letter-spacing: 1.4px;
    margin-bottom: 0px;
`;

export const Heading = styled.h1`
    
    margin-bottom: 24px;
    font-size: 80px;
    line-height: 1.1;
    font-weight: 600;
    color: ${({lightText}) => (lightText ? '#fff' : '#01bf71')};

    @media screen and (max-width: 480px)
    {
        font-size: 32px;
    }
`;

export const Subtitle = styled.p`
    max-width: 500px;
    margin-top: 60px;
    margin-bottom: 35px;
    font-size: 34px;
    line-height: 34px;
    color: ${({darkText}) => (darkText ? '#010606' : '#fff')};
`;

export const BtnWrap = styled.div`
    display: flex;
    justify-content: flex-start;
    margin-top: 80px;
    margin-left: 10px;
`;

export const ImgWrap = styled.div`
    max-width: 900px;
    height: 100%;
`;

export const Img = styled.img`
    height: 800px;
    width: 900px ;
    margin: -30px 0px -500px -500px;
    padding-right: 0;
`;

export const ImgContainer = styled.div`
    position: relative;
    width: 250px;
    height: 250px;
    top: -75px;
    left: 0px;
    z-index: 1;
    box-shadow: 0 5px 20px rgba(0,0,0,0.5);
    & h1
    {
        background: #000;
        color: #00c3ff;
        font-size: 30px;
        position: relative;
        padding-top: 10px;
        text-align: center;
        transition: 0.3s ease-in-out;
    }
    & img
    {
        max-width: 100%;
        border-radius: 5px;
        
    }
    & .imgWrap
    {
        width: fit-content;
               
    }  
    & .wrap
    {
            width: 400px;
            height: 250px;
    }
    @media screen and (max-width: 768px)
    {
        & .wrap
        {
            width: 250px;
            height: 250px;
        }
    }
`;

export const Text = styled.div`
    position: relative;
    margin-top: -140px;
    padding: 10px 0px;
    //margin-left: 20px;
    text-align: center;
    color: #fff;
    visibility: hidden;
    opacity: 0;
    transition: 0.3s ease-in-out;
    & h2
    {
        margin-bottom: 5px;
        margin-top: 8px;
        color: rgb(216, 53, 245);
        width: 105%;
    }
    & a i
    {
        margin: 20px;
        font-size: 30px;
        transition: 0.1s ease-in-out;
    }
    & a i:hover
    {
        color:rgb(205, 53, 148) ;
    }

    @media screen and (max-width: 768px)
    {
        & h2
        {
            margin-top: 40px;
            font-size: 16px;
            margin-left: -15px;
        }
        & p
        {
            margin-left: -15px;
            font-size: 14px;
        }
    }  
`;

export const CardTitle = styled.h1`
    width: 400px;
    text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
    
    @media screen and (max-width: 768px)
    {
        width: 100%;
    }  
`;
