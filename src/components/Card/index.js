import React, {useState} from 'react';
import { ButtonR } from '../ButtonElements';
import {
    CardContainer, ImgContainer,
    CardP, CardC, Text,
    CardLinker2, CardContent2, TextWrapperCard, CardContainerC, CardTitle
} from './CardElements';
import {animateScroll as scroll} from 'react-scroll';
import {
    TopLine,
    Heading,
    Subtitle,
    BtnWrap,
} from '../InfoSection/InfoElementsBig';

import LOOP_VIDEO_2 from '../../assets/video/TREE_HEADED.mp4';

import ORIGINAL_EGG from '../../assets/avatar/original_egg.png';
import GOLDEN_EGG from '../../assets/avatar/golden_egg.png';
import EMERALD_EGG from '../../assets/avatar/emerald_egg.png';
import LIMITED_EGG from '../../assets/avatar/limited_egg.png';
import ULTRARARE_EGG from '../../assets/avatar/ultrarare_egg.png';
import UNIQUE_EGG from '../../assets/avatar/unique_egg.png';
import FOUNDER_EGG from '../../assets/avatar/founder_egg.png';

const toggleTop = () => {
    scroll.scrollToTop({behavior: 'smooth', duration: '300'});
  };

const Card = ({id, topLine, 
    lightText, headline, darkText, description, 
    buttonLabel, img, alt, primary, dark, dark2}) => {
  return (
    <CardContainer id="spinningEgg">
        <CardP>
            <CardLinker2 to='/info_eggs' >
                <CardContent2 autoPlay loop muted src={LOOP_VIDEO_2} type='video/mp4' />
            </CardLinker2>  
            <TextWrapperCard>
             
                <TopLine>{topLine}</TopLine>
                <Heading lightText={lightText}>{headline}</Heading>
                <Subtitle darkText={darkText}>{description}</Subtitle>
                <BtnWrap>
                    <ButtonR to='/info_eggs' onClick={toggleTop}
                        smooth={true} duration={300}
                        spy={true}
                        exact="true"
                        offset={-80}
                        primary={primary ? 1 : 0}
                        dark={dark ? 1 : 0}
                        dark2={dark2 ? 1 : 0}>
                        <b>{buttonLabel}</b></ButtonR>
                </BtnWrap>
            </TextWrapperCard>
        </CardP> 
    </CardContainer>
    );
};

const ContentCard= () => {
    return (
        <CardContainerC id="eggsplanation">
            <CardC className='card'>
                <ImgContainer id="cont">
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{ position:"relative", margin:"auto" }}><img src={ORIGINAL_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>KUSAMA DRAGONS EGG</CardTitle>
                    </ImgContainer>
                    <Text className='text'>
                        <h2>Total supply: 110/1100</h2>
                        <p>Each egg is linked to each specific Kusama Dragon. 
                            Whenever you buy a KusamaDragon from us, you will also receive this egg.  
                            The design is the same for every egg, but they will behave differently according to the Dragon it is associated with.</p>
                    </Text>
                </CardC>
            <CardC className='card'>
                <ImgContainer>
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{ position:"relative", margin:"auto"}}><img src={GOLDEN_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>GOLDEN EGG</CardTitle>
                </ImgContainer>
                <Text className='text'>
                    <h2 >Total supply: unknown</h2>
                    <p>The rarest eggs in the collection.  
                        The Dragon Master needs to have 1 FULL SET of Kusama Dragons: 1 Limited, 1 Rare, 1 Ultra Rare,1 Legendary or Unique.
                        Supply of this type is unknown, since there are some of the original 110 Dragons still waiting to be minted.</p>
                </Text>
            </CardC>
            <CardC className='card'>
                <ImgContainer>
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{position:"relative", margin:"auto"}}><img src={EMERALD_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>EMERALD EGG</CardTitle>
                </ImgContainer>
                <Text className='text'>
                    <h2 >Total supply: 30/1100</h2>
                    <p>Employed for strategic partnership and collaborations and will bring a 3D Dragon similar of the Unique Egg.</p>
                </Text>
            </CardC>
            <CardC className='card'>
                <ImgContainer>
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{ position:"relative", margin:"auto"}}><img src={LIMITED_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>LIMITED EGG</CardTitle>
                </ImgContainer>
                <Text className='text'>
                    <h2>Total supply: 785/1100, PRICE: 1KSM</h2>
                    <p>This is the simple ticket.  
                        You will receive everything you need to enjoy the new v2NFTs collection (attribute intercompatibility, interactivity, etc…).
                        However, for each attribute, you still have the chance to get higher category attributes, such as: 
                        3 KSM tier horns , 5 KSM tier wings or even 7 KSM tier eyepatch.</p>
                </Text>
            </CardC>
            <CardC className='card'>
                <ImgContainer>
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{ position:"relative", margin:"auto"}}><img src={ULTRARARE_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>ULTRA RARE EGG</CardTitle>
                </ImgContainer>
                <Text className='text'>
                    <h2>Total supply: 175/1100, PRICE: 3KSM</h2>
                    <p>This is the premium ticket.  You
                will receive everything you need to enjoy the new v2NFTs collection (attribute intercompatibility, interactivity, etc…) plus a “safe zone” for each attribute of the Dragon. 
                In other words, with a 3 KSM egg you are sure to receive an attribute at least worth more than the 1 KSM egg.  
                However, for each attribute, you still have the chance to get higher category attributes, such as: a specific type of item usually reserved for the 5 KSM tier; or exclusive jewellery usually reserved for the 7 KSM tier.</p>
                </Text>
            </CardC>
            <CardC className='card'>
                <ImgContainer>
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{position:"relative", margin:"auto"}}><img src={UNIQUE_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>UNIQUE EGG</CardTitle>
                </ImgContainer>
                <Text className='text'>
                    <h2>Total supply: 95/1100, PRICE: 5KSM</h2>
                    <p>This is the deluxe ticket.  
                    You will receive everything you need to enjoy the new v2NFTs collection (attribute intercompatibility, interactivity, etc…) plus a “safe zone” for each attribute of the Dragon. 
                    In other words, with a 5 KSM egg you are sure to receive an attribute at least worth more than the standard for a 1 KSM and a 3KSM egg.  
                    However, for each attribute, you still have the chance to get higher category attributes. 
                    In this case your chances to receive attributes usually reserved for Founder eggs are way higher.</p>
                </Text>
            </CardC>
            <CardC className='card'>
                <ImgContainer>
                        <div className="wrap" style={{background:"#000",borderRadius:"5px"}}> 
                            <div className="imgWrap" style={{ position:"relative", margin:"auto"}}><img src={FOUNDER_EGG} alt="original_egg"/></div>
                        </div>
                        <CardTitle>FOUNDER EGG</CardTitle>
                </ImgContainer>
                <Text className='text'>
                    <h2>Total supply: 45/1100, PRICE: 7KSM</h2>
                    <p>This is the top tier. 
                    You will receive everything you need to enjoy the new v2NFTs collection (attribute intercompatibility, interactivity, etc…) and the exclusive attributes for your Dragon.</p>
                </Text>
            </CardC>
        </CardContainerC>
        );
};

export  {Card, ContentCard};
