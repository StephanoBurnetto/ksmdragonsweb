import styled from 'styled-components';
import {Link as LinkS} from 'react-scroll';
import { Link as LinkR } from 'react-router-dom';

export const Button = styled(LinkS)`
  border: 1px;
    border: 80px solid #ff33cc;
    background: ${({primary}) => (primary ? '#01BF71' : '#000')};
    white-space: nowrap;
    padding: ${({big}) => (big ? '14px 48px' : '12px 30px')};
    color: ${({dark}) => (dark ? '#010606' : '#fff')};
    font-size: ${({fontBig}) => (fontBig ? '64px' : '40px')};
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    text-decoration: none;
    animation-name: text-flicker-in-glow;
    animation-duration: 1s;
    animation-iteration-count: infinite;

    @keyframes text-flicker-in-glow {
      0% {
        opacity: 0;
      }
      10% {
        opacity: 0;
        text-shadow: none;
      }
      10.1% {
        opacity: 1;
        text-shadow: none;
      }
      10.2% {
        opacity: 0;
        text-shadow: none;
      }
      20% {
        opacity: 0;
        text-shadow: none;
      }
      20.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.25);
      }
      20.6% {
        opacity: 0;
        text-shadow: none;
      }
      30% {
        opacity: 0;
        text-shadow: none;
      }
      50% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
      }
      55% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
      }
      57.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35);
      }
      60% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35);
      }
      60.1% {
        opacity: 0;
        text-shadow: none;
      }
      65% {
        opacity: 0;
        text-shadow: none;
      }
      65.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      75% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35), 0 0 100px rgba(255, 255, 255, 0.1);
      }
 
      77.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.4), 0 0 110px rgba(255, 255, 255, 0.2), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      85% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.4), 0 0 110px rgba(255, 255, 255, 0.2), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      86.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      100% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
      }
    }
    
    transition: all 0.2s ease-in-out;
    &:hover
  {
    transition: all 0.2s ease-in-out;
    background: ${({primary}) => (primary ? '#01BF71' : '#010606')};
    animation: none;
    color: white;
    text-shadow: 0 0 30px #ff33cc;
  }
`;

export const ButtonR = styled(LinkR)`
  border: 1px;
    border: 80px solid #ff33cc;
    background: ${({primary}) => (primary ? '#01BF71' : '#000')};
    white-space: nowrap;
    padding: ${({big}) => (big ? '14px 48px' : '12px 30px')};
    color: ${({dark}) => (dark ? '#010606' : '#fff')};
    font-size: ${({fontBig}) => (fontBig ? '64px' : '40px')};
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    text-decoration: none;
    animation-name: text-flicker-in-glow;
    animation-duration: 1s;
    animation-iteration-count: infinite;

    @keyframes text-flicker-in-glow {
      0% {
        opacity: 0;
      }
      10% {
        opacity: 0;
        text-shadow: none;
      }
      10.1% {
        opacity: 1;
        text-shadow: none;
      }
      10.2% {
        opacity: 0;
        text-shadow: none;
      }
      20% {
        opacity: 0;
        text-shadow: none;
      }
      20.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.25);
      }
      20.6% {
        opacity: 0;
        text-shadow: none;
      }
      30% {
        opacity: 0;
        text-shadow: none;
      }
      50% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
      }
      55% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
      }
      57.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35);
      }
      60% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35);
      }
      60.1% {
        opacity: 0;
        text-shadow: none;
      }
      65% {
        opacity: 0;
        text-shadow: none;
      }
      65.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      75% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35), 0 0 100px rgba(255, 255, 255, 0.1);
      }
 
      77.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.4), 0 0 110px rgba(255, 255, 255, 0.2), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      85% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.4), 0 0 110px rgba(255, 255, 255, 0.2), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      86.1% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
      }
      100% {
        opacity: 1;
        text-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
      }
    }
    
    transition: all 0.2s ease-in-out;
    &:hover
  {
    transition: all 0.2s ease-in-out;
    background: ${({primary}) => (primary ? '#01BF71' : '#010606')};
    animation: none;
    color: white;
    text-shadow: 0 0 30px #ff33cc;
  }
`;