import React from 'react'
import { ServicesContainer, ServicesH1, ServicesWrapper, ServicesCard, ServicesIcon, ServicesH2, ServicesP } from './TeamCardComponents';

import AVATAR1 from '../../assets/avatar/steff_av.jpeg'
import AVATAR2 from '../../assets/avatar/princeps_av.png'
import AVATAR3 from '../../assets/avatar/alessandra_av.png'
import AVATAR4 from '../../assets/avatar/jianl_av.png'
import AVATAR5 from '../../assets/avatar/pit_av.png'
import AVATAR6 from '../../assets/avatar/nicole_av.png'
import AVATAR7 from '../../assets/avatar/ali_av.png'

const TeamCard = () => {
  return (
    <ServicesContainer id="team">
        <ServicesH1>OUR TEAM</ServicesH1>
        <ServicesWrapper id='teamwrapper'>
        <ServicesCard id='avatarr1'>
                <ServicesIcon id='avatar1'src={AVATAR1}/>
                <ServicesH2>STEFF, co-founder</ServicesH2>
                <ServicesP>F.I.R.E., Gardner and after party idol</ServicesP>
            </ServicesCard>
            <ServicesCard id='avatarr2'>
                <ServicesIcon id='avatar2' src={AVATAR2}/>
                <ServicesH2>PRINCEPS, co-founder</ServicesH2>
                <ServicesP>PhD in memes, decentralized travel agent</ServicesP>
            </ServicesCard>
            <ServicesCard id='avatarr4'>
                <ServicesIcon id='avatar4' src={AVATAR3}/>
                <ServicesH2>ALESSANDRA, 3D artist</ServicesH2>
                <ServicesP>She made those beautiful eggs... and other unreleased 3D shit</ServicesP>
            </ServicesCard>
            <ServicesCard id='avatarr4'>
                <ServicesIcon id='avatar4' src={AVATAR4}/>
                <ServicesH2>JIANL, developer</ServicesH2>
                <ServicesP>Learned how to code 2 weeks ago, he's a marijuana addicted </ServicesP>
            </ServicesCard>
            <ServicesCard id='avatarr5'>
                <ServicesIcon id='avatar5' src={AVATAR5}/>
                <ServicesH2>PIT, developer</ServicesH2>
                <ServicesP>Psychotic marijuana abuser who is also a computer genius</ServicesP>
            </ServicesCard>
            <ServicesCard id='avatarr6'>
                <ServicesIcon id='avatar6' src={AVATAR6}/>
                <ServicesH2>NICOLE, 2D artist</ServicesH2>
                <ServicesP>Gave up her sleep hours to draw tons of banners and backgrounds</ServicesP>
            </ServicesCard>
            <ServicesCard id='avatarr7'>
                <ServicesIcon id='avatar7' src={AVATAR7}/>
                <ServicesH2>ALI, 2D artist</ServicesH2>
                <ServicesP>Huge M. Jackson fan, she stole the MJ dragon from the bunker</ServicesP>
            </ServicesCard>
        </ServicesWrapper>
    </ServicesContainer>
  );
};

export default TeamCard;