import styled from 'styled-components';
import BG from '../../assets/grafiche/background_pink.jpg';


export const ServicesContainer = styled.div`
    position: relative;
    top: 0px;
    margin-top: 0px;
    margin-bottom: 00px;
    width: 1920;
    height: 830px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-image: url(${BG});

    //border-bottom: 1px solid #ff33cc;
    @media screen and (max-width: 1024px){
        height: 1400px;
        
    }
    @media screen and (max-width: 768px)
    {
        height: 1300px;
        background-image:none;
        background-color: #000;
    }
    @media screen and (max-width: 480px)
    {
        height: 1400px;
    }
`;

export const ServicesWrapper = styled.div`
    max-width: 1920px;
    margin: 0 auto;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr  1fr;
    background: Transparent;
    align-items: space-between;
    grid-gap: 50px;
    padding: 0 50px;
    //margin-left: 140px;

    @media screen and (max-width: 1280px){
        grid-template-columns: 1fr 1fr 1fr 1fr;
        
    }

    @media screen and (max-width: 1024px){
        grid-template-columns: 1fr 1fr 1fr;
        
    }
    @media screen and (max-width: 768px){
        grid-template-columns: 1fr 1fr;
       
    }
    @media screen and (max-width: 486px){
        grid-template-columns: 1fr;
    }
`;

export const ServicesCard = styled.div`
    background: white;
    width: 250px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    border-radius: 10px;
    height: 315px;
    padding: 0px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.2);
    transition: all 0.2s ease-in-out;
    border:  3px solid #000;
        

    &:hover {
        transform: scale(1.1);
        transition: all 0.2s ease-in-out;
        cursor: pointer;
    }

    @media screen and (max-width: 768px)
    {
       width: 220px;
    }
`;

export const ServicesIcon = styled.img`
    height: 200px;
    max-width: 300px;
    margin-bottom: 10px;
    background:Transparent
    
`;

export const ServicesH1 = styled.h1`
    font-size: 2.5rem;
    color: #fff;
    margin-bottom: 64px;
    background: Transparent;
    @media screen and (max-width: 768px)
    {
        font-size: 2rem;
    }
`;

export const ServicesH2 = styled.h2`
    color: #000;
    background: Transparent;
    font-size: 1rem;
    margin-bottom 10px;
    font-weight: bold;
    @media screen and (max-width: 768px)
    {
        font-size:15px;
    }
`;

export const ServicesP = styled.p`
    color: #000;
    background: Transparent;
    font-size: 1rem;
    text-align: center;
    margin-right: 2px;
    margin-left: 2px;
    @media screen and (max-width: 768px)
    {
        font-size:14px;
    }

`;
