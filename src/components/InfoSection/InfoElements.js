import styled from 'styled-components';

export const InfoContainer = styled.div`
    color: #fff;
    background: ${({lightBg}) => (lightBg ? '#f9f9f9' : '010606')};
    width: 100%;
    @media screen and (max-width: 768)
    {
        padding: 100px 0;
    }
`;
export const InfoContainerGraph = styled.div`
    color: #fff;
    background: ${({lightBg}) => (lightBg ? '#f9f9f9' : '010606')};
    width: 100%;
    height: 2440px;
    
    @media screen and (max-width: 820px)
    {
        padding: 100px 0;
        height: 1840px;
    }    
    @media screen and (max-height: 1200px)
    {
        margin-top: 50px;
    }    
    @media screen and (max-height: 900px)
    {
        margin-top: 300px;
    }   
`;

export const InfoContainerGraphHoc = styled.div`
    color: #fff;
    background: ${({lightBg}) => (lightBg ? '#f9f9f9' : '010606')};
    width: 1920;
    height: 1600px;
    @media screen and (max-width: 768px)
    {
        height: 600px;
        width: 100%;
        
       
    }    
`;

export const TrimmedInfoContainerGraph = styled.div`
    //margin-top: 100px; 
    //margin-bottom: 100px; 
    color: #fff;
    background: ${({lightBg}) => (lightBg ? '#f9f9f9' : '010606')};
    width: 100%;
    
    @media screen and (max-width: 768px)
    {
        //margin-top: -300px;
        padding: 0px 0;
        height: 500px;

        & .infocol
        {
            height: 500px;
        }
        & .wrapper
        {
            height: 500px;
        }
    }

    
`;

export const InfoWrapper = styled.div`
    display: grid;
    z-index: 1;
    height: 1000px;
    width: 100%;
    //max-width:1100px;
    margin-right: auto;
    margin-left: auto;
    padding: 0 24px;
    justify-content: center;
    @media screen and (max-width: 768px)
    {
        //height: 600px;
         & .infocol
        {
            height: 600px;
        }
    }
`;

export const TrimmedInfoWrapper = styled(InfoWrapper)`
    display: grid;
    z-index: 1;
    margin-right: auto;
    margin-left: auto;
    padding: 0 24px;
    justify-content: center;
    @media screen and (max-width: 768)
    {
    
    }

`;

export const InfoRow = styled.div`
    display: grid;
    margin-top: 100px;
    grid-auto-columns: minmax(auto, 1fr);
    align-items: center;
    grid-template-areas: ${({imgStart}) => (imgStart ? `'col2 col1'` : `'col1 col2'`)};
    
    //border-bottom: 1px #fff solid;

    @media screen and (max-width: 768px)
    {
        grid-template-areas: ${({imgStart}) => (imgStart ? `'col1' 'col2'` : `'col1 col1' 'col2 col2'`)};
        margin-top: 0px;
    }
`;
export const InfoCol = styled.div`
    display: flex;
    //grid-auto-columns: minmax(auto, 1fr);
    align-items: center;
    //grid-template-areas: ${({imgStart}) => (imgStart ? `'col2 col1'` : `'col1 col2'`)};
    
    //border-bottom: 1px #fff solid;

    @media screen and (max-width: 768px)
    {
        //grid-template-areas: ${({imgStart}) => (imgStart ? `'col1' 'col2'` : `'col1 col1' 'col2 col2'`)};
    }
`;

export const TrimInfoCol = styled.div`
    display: flex;
    //grid-auto-columns: minmax(auto, 1fr);
    align-items: center;
    //grid-template-areas: ${({imgStart}) => (imgStart ? `'col2 col1'` : `'col1 col2'`)};
    
    //border-bottom: 1px #fff solid;

    @media screen and (max-width: 768px)
    {
        //grid-template-areas: ${({imgStart}) => (imgStart ? `'col1' 'col2'` : `'col1 col1' 'col2 col2'`)};
    }
`;

export const Column1 = styled.div`
    margin-bottom: 15px;
    padding: 0 15px;
    //margin-right: 300px;
    grid-area: col1;
    //border-right: 1px solid white;
    @media screen and (max-width: 768px)
    {   
        margin-bottom: 0px;
    }
`;

export const Column2 = styled.div`
    margin-bottom: 15px;
    margin-left: 0px;
    //width: 600px;
    padding: 0 0px;
    grid-area: col2;
`;

export const TrimmedColumn1 = styled.div`
    margin-bottom: 15px;
    padding: 0 15px;
    //margin-right: 300px;
    grid-area: col1;
    //border-right: 1px solid white;
`;

export const TrimmedColumn2 = styled.div`
    margin-bottom: 15px;
    margin-left: 0px;
    //width: 600px;
    padding: 0 0px;
    grid-area: col2;
`;

export const TextWrapper = styled.div`
    width: 100%;
    padding-top: 0px;
    padding-bottom: 150px;
    margin-left: 0px;
    
    @media screen and (max-width: 768px)
    {   
        padding-bottom: 70px;
    }
`;

export const TopLine = styled.p`
    color:#01bf71;
    font-size: 16px;
    line-height: 16px;
    font-weight: 700;
    letter-spacing: 1.4px;
    //text-transform: uppercase;
    margin-bottom: 0px;
    text-align:right;
    @media screen and (max-width: 768px)
    {
        text-align: center;
        margin-bottom: 5px;
    }
`;

export const Heading = styled.h1`

    margin-bottom: 24px;
    font-size: 48px;
    line-height: 1.1;
    text-align:right;
    font-weight: 600;
    color: ${({lightText}) => (lightText ? '#fff' : '#01bf71')};

    @media screen and (max-width: 768px)
    {
        font-size: 32px;
        text-align: center;
    }
`;

export const Subtitle = styled.p`
    //align-item: flex-end;
    text-align: right;
    //max-width: 440px;
    margin-left: 100px;
    margin-bottom: 35px;
    font-size: 18px;
    line-height: 24px;
    color: ${({darkText}) => (darkText ? '#01bf71' : '#fff')};
    @media screen and (max-width: 768px)
    {
        text-align: center;
        margin-left: 0px;
        margin-bottom: 0px;
    }
`;

export const BtnWrap = styled.div`
    display: flex;
    justify-content: flex-start;
    //margin-top: 80px;
    //margin-left: 10px;
`;

export const ImgWrap = styled.div`
    max-width: 550px;
    height: 100%;
    
`;

export const TrimmedImgWrap = styled.div`
    //max-width: 550px;
    //height: 100%;
`;

export const Img = styled.img`
    //height: 800px;
    width: 100% ;
    margin: 0px 0px 100px 0px;
    padding-right: 0;
    
`;

export const TrimmedImg = styled.img`
    //height: 800px;
    width: 150% ;
    margin: 0px 0px 100px 0px;
    padding: 100px;
    
`;

export const ImgGraph = styled.img`
    //height: 800px;
    width: 160% ;
    margin: 0px 0px 100px -100px;
    padding-right: 0;
    
    @media screen and (max-width: 768px)
    {   
        width: 100%;
        margin: 0px 0px 0px 0px;
    }
`;


export const Video = styled.video`
    width: 110%;
    height: 110%;
    -o-object-fit: cover;
    object-fit: cover;
    margin: 0px 0px 100px 0px;
    border-left:  1px solid #ff33cc;
    @media screen and (max-width: 820px)
    {   
        width: 386px;
        height: 100%;
        margin: 0px 0px 0px 0px;
    }

    @media screen and (max-width: 768px)
    {   
        width: 100%;
        height: 100%;
        margin: 0px 0px 0px 0px;
    }
`;