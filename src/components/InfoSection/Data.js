export const homeObjOne = {
    id: 'about', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Original Kusama Dragons',
    headline: '110 HANDMADE CREATURES',
    description: '5 Categories: Limited, Rare, Ultra Rare, Legendary and Unique.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/finiti/stoned_dragon.png'),
    alt: 'infoGraph',
    dark: false,
    primary: false,
    darkText: false
};

export const homeObjTwo = {
    id: 'project', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Ambitious and pioneering NFT project operating on RMRK',
    headline: 'A HUB OF ARTISTS THAT PRODUCES INNOVATIVE CONTENT IN STEP WITH THE TIMES, IN CONTINUOUS EVOLUTION',
    description: 'The original 110 Dragons are intended as “shares” of this artistic project. The owners of the original Dragon fleet will then indefinitely receive the artistic dividends of the project in the form of new NFTs. In the Dragon Citadel, utility and experience come first. Travel, experiences, passive income from NFTs, an increasingly lively community and much more is awaiting you.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    vid: require('../../assets/video/carousel.mp4'),
    alt: 'infoGraph',
    dark: false,
    primary: false,
    darkText: false
};

export const homeObjTree = {
    id: 'features', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Original Kusama Dragons',
    headline: '110 UNIQUE PIECES',
    description: 'Only developed for strategic collaborations. Not sold, expecially though for community building.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/grafiche/wholedragon_bene_bene.jpg'),
    alt: 'info_features',
    dark: false,
    primary: false,
    darkText: false
};

export const eggObjOne = {
    id: 'home_0', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Weekly drops',
    headline: 'Eggs will be the only way to receive one of these 3D Dragons. NO future auction/releases.',
    description: 'Weekly drops of 3, 4, 5, 6 until sold out or no more demand with subsequent burn of the unsold. Total supply of eggs (sold + airdrops) will determine the total supply of the v2NFTs Dragon collection. ',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/grafiche/weekly_drops.jpg'),
    alt: 'info_features',
    dark: false,
    primary: false,
    darkText: false
};

export const eggObjTwo = {
    id: 'eggsplanation', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Original Kusama Dragons',
    headline: '110 UNIQUE PIECES',
    description: 'Only developed for strategic collaborations. Not sold, expecially though for community building.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/grafiche/first_faq.jpg'),
    alt: 'info_features',
    dark: false,
    primary: false,
    darkText: false
};

export const eggObjTree = {
    id: 'eggsplanation', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Original Kusama Dragons',
    headline: '110 UNIQUE PIECES',
    description: 'Only developed for strategic collaborations. Not sold, expecially though for community building.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/grafiche/second_faq_info.jpg'),
    alt: 'info_features',
    dark: false,
    primary: false,
    darkText: false
};

export const eggObjFour = {
    id: 'eggsplanation', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Original Kusama Dragons',
    headline: '110 UNIQUE PIECES',
    description: 'Only developed for strategic collaborations. Not sold, expecially though for community building.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/grafiche/only_tird.jpg'),
    alt: 'info_features',
    dark: false,
    primary: false,
    darkText: false
};

export const eggObjFive = {
    id: 'eggsplanation', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Original Kusama Dragons',
    headline: '110 UNIQUE PIECES',
    description: 'Only developed for strategic collaborations. Not sold, expecially though for community building.',
    buttonLabel: 'GET STARTED',
    imgStart: false,
    img: require('../../assets/grafiche/only_fourth.jpg'),
    alt: 'info_features',
    dark: false,
    primary: false,
    darkText: false
};

export const wildCard = {
    id: 'spin', 
    lightBg: false,
    lightText: true,
    lightTextDesc: true, 
    topLine: 'Max Supply',
    headline: '1100 DRAGON EGGS',
    description: '7 TYPES OF THEM',
    buttonLabel: 'MORE ABOUT',
    imgStart: false,
    img: require('/'),
    alt: 'infoGraph',
    dark: false,
    primary: false,
    darkText: true
};