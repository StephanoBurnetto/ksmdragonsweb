import styled from 'styled-components';

export const InfoContainer = styled.div`
    color: #fff;
    background: ${({lightBg}) => (lightBg ? '#f9f9f9' : '010606')};
    width: 100%;
    margin-top: 200px;
    @media screen and (max-width: 768)
    {
        padding: 0px 0;
        margin-top: 100px;
    }
    
`;

export const InfoWrapper = styled.div`
    display: grid;
    z-index: 1;
    height: 1060px;
    width: 100%;
    //max-width: 1100px;
    margin-right: auto;
    margin-left: auto;
    padding: 0 24px;
    justify-content: center;
`;

export const InfoRow = styled.div`
    display: grid;
    grid-auto-columns: minmax(auto, 1fr);
    align-items: center;
    grid-template-areas: ${({imgStart}) => (imgStart ? `'col2 col1'` : `'col1 col2'`)};
    
    @media screen and (max-width: 900px)
    {
        grid-template-areas: ${({imgStart}) => (imgStart ? `'col1' 'col2'` : `'col1 col1' 'col2 col2'`)};
    }
`;

export const Column1 = styled.div`
    margin-bottom: 300px;
    margin-right: 100px;
    padding: 0 0px;
    grid-area: col1;
    @media screen and (max-width: 820px)
    {
        margin-bottom: 150px;
        margin-right: 50px;
    }
    @media screen and (max-width: 480px)
    {
        margin-bottom: 0px;
        margin-right: 0px;
    }
`;

export const Column2 = styled.div`
    margin-bottom: 0px;
    margin-left: 0;
    width: 100%;
    padding: 0 0px;
    grid-area: col2;
    
`;

export const TextWrapper = styled.div`
    max-width: 1000px;
    padding-top: 0;
    padding-bottom: 60px;
    @media screen and (max-width: 820px)
    {
        text-align: center;
    }
`;

export const TopLine = styled.p`
    color:#01bf71;
    font-size: 32px;
    line height: 16px;
    font-weight: 700;
    letter-spacing: 1.4px;
    //text-transform: uppercase;
    margin-bottom: 0px;
`;

export const Heading = styled.h1`
    
    margin-bottom: 24px;
    font-size: 70px;
    line-height: 1.1;
    font-weight: 600;
    color: ${({lightText}) => (lightText ? '#fff' : '#01bf71')};

    @media screen and (max-width: 480px)
    {
        font-size: 32px;
    }
`;

export const Subtitle = styled.p`
    max-width: 500px;
    margin-top: 60px;
    margin-bottom: 35px;
    font-size: 34px;
    line-height: 34px;
    color: ${({darkText}) => (darkText ? '#01bf71' : '#fff')};
    @media screen and (max-width: 820px)
    {
        margin-left: 136px;
    }
    @media screen and (max-width: 767px)
    {
        margin-left: auto;
    }
`;

export const BtnWrap = styled.div`
    margin-top: 80px;
    margin-left: 10px;
    @media screen and (min-width: 821px)
    {
        display: flex;
        justify-content: flex;
    }
`;

export const ImgWrap = styled.div`
    width: 100%;
    height: 100%;
`;

export const Img = styled.img`
    height: 100%;
    width: 100% ;
    margin: 0px 0px 0px 0px;
    padding-right: 0;
`;
