import React from 'react';
import { Button } from '../ButtonElements';

import {
    InfoContainer as InfoContainerBig,
    InfoWrapper as InfoWrapperBig,
    InfoRow as InfoRowBig,
    Column1 as Column1Big,
    Column2 as Column2Big,
    TextWrapper as TextWrapperBig,
    TopLine as TopLineBig,
    Heading as HeadingBig,
    Subtitle as SubtitleBig,
    BtnWrap as BtnWrapBig,
    ImgWrap as ImgWrapBig, 
    Img as ImgBig
} from './InfoElementsBig';

import {
    InfoContainer, InfoContainerGraph,
    InfoWrapper,
    InfoRow, InfoCol, 
    Column1, Column2,
    TextWrapper,
    TopLine, Heading, Subtitle,
    BtnWrap, ImgWrap, Img, Video, ImgGraph,
    TrimmedInfoContainerGraph, TrimmedInfoWrapper, TrimmedColumn1, TrimmedColumn2,
    TrimmedImgWrap, TrimmedImg, InfoContainerGraphHoc, TrimInfoCol
} from './InfoElements';



const InfoSectionBig = ({lightBg, id, imgStart, topLine, 
                lightText, headline, darkText, description, 
                buttonLabel, img, alt, primary, dark, dark2}) => {
  return(
      <>
        <InfoContainerBig lightBg= {lightBg} id={id}>
            <InfoWrapperBig>
                <InfoRowBig imgStart={imgStart}>
                    <Column1Big id ="col">
                    <ImgWrapBig>
                            <ImgBig src={img} alt={alt}/>
                    </ImgWrapBig>                        
                    </Column1Big>
                    <Column2Big>
                    <TextWrapperBig>
                            <TopLineBig>{topLine}</TopLineBig>
                            <HeadingBig lightText={lightText}>{headline}</HeadingBig>
                            <SubtitleBig darkText={darkText}>{description}</SubtitleBig>
                            <BtnWrapBig id ="big">
                                <Button to='services' className="btn"
                                smooth={true} duration={300}
                                spy={true}
                                exact="true"
                                offset={-80}
                                primary={primary ? 1 : 0}
                                dark={dark ? 1 : 0}
                                dark2={dark2 ? 1 : 0}>
                                    <b>{buttonLabel}</b></Button>
                            </BtnWrapBig>
                        </TextWrapperBig>
                    </Column2Big>
                </InfoRowBig>
            </InfoWrapperBig>
        </InfoContainerBig>
      </>
  );
};

const InfoSection = ({lightBg, id, imgStart, topLine, 
    lightText, headline, darkText, description, 
    buttonLabel, img, alt, primary, dark, dark2}) => {
    return(
        <>
          <InfoContainer lightBg= {lightBg} id={id}>
              <InfoWrapper>
                  <InfoRow imgStart={imgStart}>
                      <Column1>
                      <TextWrapper>
                              <TopLine>{topLine}</TopLine>
                              <Heading lightText={lightText}>{headline}</Heading>
                              <Subtitle darkText={darkText}>{description}</Subtitle>
                          </TextWrapper>                
                      </Column1>
                      <Column2>
                      <ImgWrap>
                              <Img src={img} alt={alt}/>
                      </ImgWrap> 
                      </Column2>
                  </InfoRow>
              </InfoWrapper>
          </InfoContainer>
        </>
    );
};

const VideoInfoSection = ({lightBg, id, imgStart, topLine, 
    lightText, headline, darkText, description, 
    buttonLabel, vid, alt, primary, dark, dark2}) => {
    return(
        <>
          <InfoContainer lightBg= {lightBg} id={id}>
              <InfoWrapper id="wr">
                  <InfoRow imgStart={imgStart}>
                      <Column1>
                      <TextWrapper>
                              <TopLine>{topLine}</TopLine>
                              <Heading lightText={lightText}>{headline}</Heading>
                              <Subtitle darkText={darkText}>{description}</Subtitle>
                          </TextWrapper>                
                      </Column1>
                      <Column2 id="col2">
                      <ImgWrap id="imgwrap">
                        <Video autoPlay muted loop src={vid} type='video/mp4'/>
                      </ImgWrap> 
                      </Column2>
                  </InfoRow>
              </InfoWrapper>
          </InfoContainer>
        </>
    );
};

const InfoGraphicSection = ({lightBg, id, imgStart, topLine, 
    lightText, headline, darkText, description, 
    buttonLabel, img, alt, primary, dark, dark2}) => {
    return(
        <>
          <InfoContainerGraph id="aa" lightBg= {lightBg} id={id}>
              <InfoWrapper id="wrap">
                  <InfoCol imgStart={imgStart}>
                      <Column2>
                      <ImgWrap>
                              <ImgGraph src={img} alt={alt} />
                      </ImgWrap> 
                      </Column2>
                  </InfoCol>
              </InfoWrapper>
          </InfoContainerGraph>
        </>
    );
};

const TrimmedImageSection = ({lightBg, id, imgStart, topLine, 
    lightText, headline, darkText, description, 
    buttonLabel, img,  alt, primary, dark, dark2}) => {
    return(
        <>
        <InfoContainerGraphHoc className= "conthoc"lightBg= {lightBg} id={id}>
            <InfoWrapper className="infoWrap">
                <InfoCol className="infocol" imgStart={imgStart}>
                    <Column2 className="col">
                    <ImgWrap>
                            <ImgGraph src={img} alt={alt} />
                    </ImgWrap> 
                    </Column2>
                </InfoCol>
            </InfoWrapper>
        </InfoContainerGraphHoc>
      </>
  );

};


const TrimmedInfoGraphicSection = ({lightBg, id, imgStart, topLine, 
    lightText, headline, darkText, description, 
    buttonLabel,  img, alt, primary, dark, dark2}) => {
    return(
        <>
          <TrimmedInfoContainerGraph lightBg= {lightBg} id={id}>
              <TrimmedInfoWrapper id="wr" className="wrapper">
                  <TrimInfoCol imgStart={imgStart} id="col" className="infocol" >
                      <Column2>
                      <ImgWrap>
                              <ImgGraph src={img} alt={alt} />
                      </ImgWrap> 
                      </Column2>
                  </TrimInfoCol>
              </TrimmedInfoWrapper>
          </TrimmedInfoContainerGraph>
        </>
    );
};

export  { 
    InfoSection, TrimmedImageSection,
    InfoSectionBig, InfoGraphicSection, VideoInfoSection, TrimmedInfoGraphicSection};
