import React from 'react';
import {
    SidebarContainer,
    Icon,
    CloseIcon,
    SidebarWrapper,
    SidebarMenu,
    SidebarLink,
    SideBtnWrap, 
    SidebarRoute
} from './SidebarElements';


const Sidebar = ({isOpen, toggle}) => {
  return (
    <SidebarContainer isOpen={isOpen} onClick={toggle}>
        <Icon onClick={toggle}>
            <CloseIcon />
        </Icon>
        <SidebarWrapper>
            <SidebarMenu>
                
                <SidebarRoute to="DS"> DISCORD </SidebarRoute>
                <SidebarRoute to="SINGULAR"> SINGULAR </SidebarRoute>
                <SidebarRoute to="TW"> TWITTER </SidebarRoute>
            </SidebarMenu>
        </SidebarWrapper>
    </SidebarContainer>
  );
};

export default Sidebar;


/*

    <SidebarRoute to="TBA"> EXPERIENCE </SidebarRoute>
                <SidebarRoute to="TBA"> BIDS </SidebarRoute>
                <SidebarRoute to="TBA"> MERCH </SidebarRoute>
*/