import React from 'react'
import {animateScroll as scroll} from 'react-scroll';
import {
    FooterContainer, FooterWrap, FooterLinksContainer, FooterLinksWrapper, FooterLinkItems,
    FooterLinkTitle, FooterLink, FooterBtn, SocialMedia, SocialMediaWrap, SocialLogo, SocialIcons, SocialIconLink, WebsiteRights
} from './FooterElements';

import {
  FaTwitter, FaDiscord
} from 'react-icons/fa';

import {
  FiMail
} from 'react-icons/fi';

  const discord_link = 'https://discord.gg/rrg7h7VJg3';
  const twitter_link = 'https://twitter.com/KusamaDragons';
  const singular_link = 'https://singular.rmrk.app/collections/ecfe133cb8fdb0732a-KUSAMADRAGONS';
  const mail_link = 'mailto:Kusamadragons@gmail.com';
  const toggleHome = () => {
    scroll.scrollToTop({behavior: 'smooth', duration: '300'});
  };

const Footer = () => {
  return (
    <FooterContainer>
        <FooterWrap>
            <FooterLinksContainer>
                <FooterLinksWrapper>
                    <FooterBtn as='a' href={singular_link} target="_blank"> <b>GO TO COLLECTION</b> </FooterBtn> 
                </FooterLinksWrapper>
            </FooterLinksContainer>
            <SocialMedia>
              <SocialMediaWrap>
                <SocialLogo to='/' onClick={toggleHome}>
                  Kusama Dragons
                </SocialLogo>
                <WebsiteRights>KusamaDragons © {new Date().getFullYear()} All rights reserved.
                </WebsiteRights>
                <SocialIcons>
                  <SocialIconLink href={twitter_link} target="_blank" aria-label="Twitter">
                    <FaTwitter/>
                  </SocialIconLink>
                  <SocialIconLink href={discord_link} target="_blank" aria-label="Discord">
                    <FaDiscord/>
                  </SocialIconLink>
                  <SocialIconLink href={mail_link} target="_blank" aria-label="Singular">
                    <FiMail/>
                  </SocialIconLink>
                </SocialIcons>
              </SocialMediaWrap>
            </SocialMedia>
        </FooterWrap>
    </FooterContainer>
  );
};

export default Footer;

//Items row wrapper column

/*
  <FooterLinkItems>
                        <FooterLinkTitle>OUR SOCIAL</FooterLinkTitle>
                        <FooterLink as="a" href={discord_link} target="_blank">Discord</FooterLink>
                        <FooterLink as="a" href={twitter_link} target="_blank">Twitter</FooterLink>
                        <FooterLink as="a" href={mail_link} target="_blank">Kusamadragons@gmail.com</FooterLink>
                    </FooterLinkItems>
*/