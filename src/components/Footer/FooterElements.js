import styled from 'styled-components';
import { Link as LinkR } from 'react-router-dom';

export const FooterContainer = styled.footer`
    background-color: #000;
`;

export const FooterWrap = styled.div`
    padding: 48px 24px;
    background-color: black;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin 0 auto;
    @media screen and (max-width: 820px) {
      margin-top: 600px;
    }

`;

export const FooterLinksContainer = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;
    @media screen and (max-width: 820px) {
        padding-top: 32px;
    }
`;

export const FooterLinksWrapper = styled.div`
    display: flex;
    
    @media screen and (max-width: 820px) {
        flex-direction: column;
    }
`;

export const FooterLinkItems = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin: 16px;
    text-align: left;
    width: 160px;
    box-sizing: border-box;
    color: #fff;
    padding: 0 0 0 10px;
    border-left: 1px solid white;


    @media screen and (max-width: 420px) {
        margin: 0;
        padding: 10px;
        width: 100%;
    }
`;

export const FooterLinkTitle = styled.h1`
    font-size: 24px;
    margin-bottom: 16px;
    background-color: transparent;
`;

export const FooterLink = styled(LinkR)`
    color: #fff;
    text-decoration: none;
    margin-bottom: 0.5rem;
    font-size: 18px;
    padding: 2px;
    &:hover {
        color: #01bf71;
        transition: 0.3s ease-out;
        margin-left: 2px;
        border-right:  1px solid #ff33cc;
    }
`;
export const FooterBtn = styled(LinkR)`
    background: ${({primary}) => (primary ? '#01BF71' : '#000')};
    white-space: nowrap;
    padding: ${({big}) => (big ? '14px 48px' : '12px 30px')};
    color: ${({dark}) => (dark ? '#010606' : '#fff')};
    font-size: ${({fontBig}) => (fontBig ? '64px' : '40px')};
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    padding: 0px 30px 0px 0px;//0px 100px 0px 0px;
    justify-content: center;
    align-items: center;
    @media screen and (max-width: 420px) {
      font-size: 32px;
      padding: 0px 0px 0px 0px;//0px 100px 0px 0px;
  }

    text-decoration: none;
    transition: all 0.2s ease-in-out;
    animation-name: text-flicker-in-glow;
    animation-duration: 1s;
    animation-iteration-count: infinite;

    @keyframes text-flicker-in-glow {
        0% {
          opacity: 0;
        }
        10% {
          opacity: 0;
          text-shadow: none;
        }
        10.1% {
          opacity: 1;
          text-shadow: none;
        }
        10.2% {
          opacity: 0;
          text-shadow: none;
        }
        20% {
          opacity: 0;
          text-shadow: none;
        }
        20.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.25);
        }
        20.6% {
          opacity: 0;
          text-shadow: none;
        }
        30% {
          opacity: 0;
          text-shadow: none;
        }
        30.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
        }
        30.5% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
        }
        30.6% {
          opacity: 0;
          text-shadow: none;
        }
        45% {
          opacity: 0;
          text-shadow: none;
        }
        45.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
        }
        50% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
        }
        55% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.45), 0 0 60px rgba(255, 255, 255, 0.25);
        }
        55.1% {
          opacity: 0;
          text-shadow: none;
        }
        57% {
          opacity: 0;
          text-shadow: none;
        }
        57.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35);
        }
        60% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35);
        }
        60.1% {
          opacity: 0;
          text-shadow: none;
        }
        65% {
          opacity: 0;
          text-shadow: none;
        }
        65.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35), 0 0 100px rgba(255, 255, 255, 0.1);
        }
        75% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.35), 0 0 100px rgba(255, 255, 255, 0.1);
        }
        75.1% {
          opacity: 0;
          text-shadow: none;
        }
        77% {
          opacity: 0;
          text-shadow: none;
        }
        77.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.4), 0 0 110px rgba(255, 255, 255, 0.2), 0 0 100px rgba(255, 255, 255, 0.1);
        }
        85% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.55), 0 0 60px rgba(255, 255, 255, 0.4), 0 0 110px rgba(255, 255, 255, 0.2), 0 0 100px rgba(255, 255, 255, 0.1);
        }
        85.1% {
          opacity: 0;
          text-shadow: none;
        }
        86% {
          opacity: 0;
          text-shadow: none;
        }
        86.1% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
        }
        100% {
          opacity: 1;
          text-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
        }
      }
      

    &:hover
    {
        transition: all 0.2s ease-in-out;
        background: ${({primary}) => (primary ? '#01BF71' : '#010606')};
        color: white;
        text-shadow: 0 0 30px #ff33cc;
        animation: none;
    }
`;

export const SocialMedia = styled.section`
  max-width: 1000px;
  width: 100%;
`;

export const SocialMediaWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1100px;
  margin: 40px auto 0 auto;

  @media screen and (max-width: 820px) {
    flex-direction: column;
  }
`;

export const SocialLogo = styled(LinkR)`
  color: #fff;
  justify-self: start;
  cursor: pointer;
  text-decoration: none;
  font-size: 1.5rem;
  display: flex;
  align-items: center;
  margin-bottom: 16px;
  font-weight: bold;
  &:hover
  {
      transition: all 0.2s ease-in-out;
      color: #ff33cc;
  }
`;

export const WebsiteRights = styled.small`
  color: #fff;
  margin-bottom: 16px;
  text-align: center;
`;

export const SocialIcons = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 240px;
  margin-bottom: 10px;
`;

export const SocialIconLink = styled.a`
  color: #fff;
  font-size: 24px;
  &:hover
  {
      transition: all 0.2s ease-in-out;
      color: #ff33cc;
  }
`;