import React from 'react'
import { ServicesContainer, ServicesH1, ServicesWrapper, ServicesCard, ServicesIcon, ServicesH2, ServicesP } from './ServicesElements';

import Icon1 from '../../assets/logos/serv1.png';
import Icon2 from '../../assets/logos/serv2.png';
import Icon3 from '../../assets/logos/serv3.png';

const Services = () => {
  return (
    <ServicesContainer id="services">
        <ServicesH1></ServicesH1>
        <ServicesWrapper id='wrapper'>
        <ServicesCard id='card1' to='project' smooth={true} duration={300}
                                spy={true}
                                exact="true"
                                offset={-80}>
                <ServicesIcon id='icon3'src={Icon3}/>
                <ServicesH2>PROJECT</ServicesH2>
                <ServicesP>Dragons lore and project description.</ServicesP>
            </ServicesCard>
            <ServicesCard id='card2' to='features' smooth={true} duration={300}
                                spy={true}
                                exact="true"
                                offset={-80}>
                <ServicesIcon id='icon1' src={Icon1}/>
                <ServicesH2>FEATURES</ServicesH2>
                <ServicesP>Advantages of being a dragon holder and other features.</ServicesP>
            </ServicesCard>
            <ServicesCard id='card3' to='team' smooth={true} duration={300}
                                spy={true}
                                exact="true"
                                offset={-80}>
                <ServicesIcon id='icon2' src={Icon2}/>
                <ServicesH2>TEAM</ServicesH2>
                <ServicesP>Know the members of the team!</ServicesP>
            </ServicesCard>
        </ServicesWrapper>
    </ServicesContainer>
  );
};

export default Services;
