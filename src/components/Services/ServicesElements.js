import styled from 'styled-components';
import { Link as LinkS } from 'react-scroll';

export const ServicesContainer = styled.div`
    margin-top: 0px;
    margin-bottom: 100px;
    height: 100%;
    width: 100%;
    position: relative;
    
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: #010606;

    //border-top: 1px solid #ff33cc;
    //border-bottom: 1px solid #ff33cc;

`;

export const ServicesWrapper = styled.div`
    //max-width: 1920px;
    
    margin: 0 auto;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    background: #000;
    align-items: center;
    grid-gap: 16px;
    padding: 50px 50px;
    overflow: hidden;
    //margin-left: 140px;
    @media screen and (max-width: 1000px){
        grid-template-columns: 1fr 1fr;
    }
    @media screen and (max-width: 768px){
        grid-template-columns: 1fr;
    }
`;

export const ServicesCard = styled(LinkS)`
    background: #000;
    width: 100%
    max-width: 500px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    border-radius: 10px;
    height: 340px;
    padding: 30px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.2);
    transition: all 0.2s ease-in-out;
    text-decoration: none;
    &:hover {
        transform: scale(1.1);
        transition: all 0.2s ease-in-out;
        cursor: pointer;
        border-right:  3px solid #ff33cc;
        border-top:  3px solid #ff33cc;
    }
`;

export const ServicesIcon = styled.img`
    height: 200px;
    max-width: 400px;
    margin-bottom: 10px;
`;

export const ServicesH1 = styled.h1`
    font-size: 2.5rem;
    color: #fff;
    margin-bottom: 64px;

     {
        font-size: 2rem;
    }
`;

export const ServicesH2 = styled.h2`
    color: #fff;
    background: #000;
    font-size: 1rem;
    margin-bottom 10px;
`;

export const ServicesP = styled.p`
    color: #fff;
    background: #000;
    font-size: 1rem;
    text-align: center;
`;