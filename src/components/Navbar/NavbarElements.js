import styled from "styled-components";
import { Link as LinkR } from 'react-router-dom';
import { Link as LinkS } from 'react-scroll';

export const Nav = styled.nav`
  background: #000;
  height: 80px;
  //margin-top: -80px; potrebbe dover essere decommentato
  display: flex;
  justify-content: center;
  align-items: center;
  //flex-direction: column;
  font-size: 1rem;
  position: sticky;
  top: 0;
  z-index: 999;
  width: 1920;
  @media screen and (max-width: 900px)
  {
    transition: 0.8s all ease;
    margin-top: -80px; 
    width: 100%;
  }
`;

export const NavbarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  height: 80px;
  width: 100%;
  z-index: 1;
  padding: 0 24px;
  
`;

export const NavLogo= styled(LinkR)`
  color: red;
  justify-self: flex-start;
  cursor: pointer;
  font-size: 1.5rem;
  display: flex:
  align-items: center;
  margin-left: -80px;
  font-weight: bold;
  text-decoration: none;
  @media screen and (max-width: 1000px)
    {
      //display: none;
    }
`;

export const LogoImg = styled.img`
    height: 80px;
    width: 400px;
    background:Transparent;
`;

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 900px)
  {
    display: flex;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(0%, 60%);
    font-size: 1.8rem;
    cursor: pointer;
    color: #fff;
  }
`;

export const NavMenu = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  text-align: center;
  margin-right: 22px;
  @media screen and (max-width: 900px)
  {
    display: none;
  }
`;

export const NavItem = styled.li`
  height: 80px;
  padding-left: 60px;
  padding-right: 60px;
  transition: all 0.2s ease-in-out;
  &:hover
  {
    transition: all 0.2s ease-in-out;
    border-bottom:  1px solid #ff33cc;
  }

  
`;

export const NavLinks = styled(LinkS)`
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;

  transition: all 0.2s ease-in-out;
  &:hover
  {
    color: #ff33cc;
  }
  &.active
  {
    border.bottom: 3px solid #ff33cc;
  }

  
`;

export const NavBtn = styled.nav`
  display:flex;
  align-item: center;
  
  overflow: hidden;
  outline: none;
  transition: all 0.2s ease-in-out;

  &:hover
  {
    transition: all 0.2s ease-in-out;
    border-bottom:  1px solid #ff33cc;
  }

  @media screen and (max-width; 768px)
  {
    display: none;
  }
`;

export const NavBtnLink = styled(LinkR)`
  border: 100px;
  white-space: nowrap;
  padding: 22px 22px;
  font-size: 16px;
  
  border:none;
  overflow: hidden;
  outline: none;
  cursor: pointer;
`;