//qui dentro si scrive una funzione js che crea il componente
import React, {useState} from 'react';
import {FaBars} from 'react-icons/fa';
import {animateScroll as scroll} from 'react-scroll';
import Alert from 'react-bootstrap/Alert';
import { 
  Nav, 
  NavbarContainer, 
  NavLogo, 
  MobileIcon,
  NavMenu,
  NavItem,
  NavLinks,
  NavBtn,
  NavBtnLink,
  LogoImg
}
from './NavbarElements';

import DRG_LOGO from '../../assets/logos/NEON_PINK_.png'
import SINGULAR_LOGO from '../../assets/logos/SINGULAR_LOGO_STRONG.svg'
import DS_LOGO from '../../assets/logos/DS_LOGO_STRONG.svg'
import TWITTER_LOGO from '../../assets/logos/TWITTER_LOGO_STRONG.svg'

const Navbar = ({ toggle }) => {
  const [show, setShow] = useState(false);

  const discord_link = 'https://discord.gg/rrg7h7VJg3';
  const twitter_link = 'https://twitter.com/KusamaDragons';
  const singular_link = 'https://singular.rmrk.app/collections/ecfe133cb8fdb0732a-KUSAMADRAGONS';

  const toggleHome = () => {
    scroll.scrollToTop({behavior: 'smooth', duration: '300'});
  };

  return( 
      <>
        <Nav>
          <NavbarContainer>
            <NavLogo to='/'  onClick={toggleHome} >
              <LogoImg src={DRG_LOGO} width = "400" height="80" alt="logo"/>
            </NavLogo>
            <NavMenu>
            <NavItem>
                <NavLinks to="TBA" onClick={() => setShow(true)}>EXPERIENCE</NavLinks>
              </NavItem>
              <NavItem>
                <NavLinks to="TBA" onClick={() => setShow(true)}>BIDS</NavLinks>
              </NavItem>
              <NavItem>
                <NavLinks to="TBA" onClick={() => setShow(true)}>MERCH </NavLinks>
              </NavItem>
            </NavMenu>
            <MobileIcon onClick={toggle}>
              <FaBars />
            </MobileIcon>
            <NavMenu>
            <NavBtn>
                <NavBtnLink as="a" href={singular_link} target="_blank">
                  <img src={SINGULAR_LOGO} width = "100" height="40" alt="logo"></img>
                </NavBtnLink>
              </NavBtn>
              <NavBtn>
                <NavBtnLink as="a" href={discord_link} target="_blank">
                  <img src={DS_LOGO} width = "80" height="40" alt='logo'></img>
                </NavBtnLink>
              </NavBtn>
              <NavBtn>
                <NavBtnLink as="a" href={twitter_link} target="_blank">
                  <img src={TWITTER_LOGO} width = "60" height="40"  alt="logo"></img>
                </NavBtnLink>
              </NavBtn>
            </NavMenu>
          </NavbarContainer>
        </Nav>
        <Alert show={show} variant="warning" onClose={() => setShow(false)} dismissible style={{position:"fixed", textAlign:"center", width:"100%", zIndex: "999", top:"70px"}}>
          TBA! More features to come!
        </Alert>
      </>
        );
};

export default Navbar;