import styled from "styled-components";
import {MdArrowForward, MdKeyboardArrowRight} from 'react-icons/md';
export const HeroContainer = styled.div`
    background: #000;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 30px; //top n boot 0 left n right 30
    width: 1920;
    
    position: relative;
    z-index: 1;
    @media screen and (max-width: 1280px)
    {
        margin-top: 80px;
    } 
    @media screen and (max-width: 600px)
    {
        width: 100%;
    }    
    @media screen and (max-width: 400px)
    {
        width: 100%;
    }    
   
`;

export const HeroBg = styled.div`
    position: relative;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    //width: 100%;
    //overflow: hidden;

    @media screen and (max-width: 1280px)
    {
        margin-top: 200px;
    } 

`;

export const VideoBg = styled.video`
    -o-object-fit: cover;
    object-fit: cover;
    background: black;
    width: 1900px;
    height: 850px;

    @media screen and (max-width: 1280px)
    {
        display:none
    }    

   
`;
export const VideoBgMini = styled.video`
    -o-object-fit: cover;
    object-fit: cover;
    background: black;
    display: none;
    @media screen and (max-width: 480px)
    {
        font-size: 32px;
    }    
`;
export const HeroContent = styled.div`
    z-index: 3;
    max-width: 1920;
    
    position: relative;
    padding: 8px 24px;
    display: flex;
    flex-direction: column;
    align-items: center;
    background: transparent;
    bottom: -100px;
`;

export const HeroH1 = styled.h1`
    coor: #fff;
    font-size: 48px;
    text-align: center;

    @media screen and (max-width: 768px)
    {
        font-size: 40px;
    }

    @media screen and (max-width: 480px)
    {
        font-size: 32px;
    }    
`;

export const HeroP = styled.p`
    margin-top: 24px;
    color: #fff;
    font-size: 24px;
    text-align: center;
    max-width: 600px;

    @media screen and (max-width: 768px)
    {
        font-size: 24px;
    }

    @media screen and (max-width: 480px)
    {
        font-size: 18px;
    }   
`;

export const HeroBtnWrapper = styled.div`
    margin-top: 32px;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const ArrowForward = styled(MdArrowForward)`
    margin-left: 8px;
    font-size: 20px;
`;

export const ArrowRight = styled(MdKeyboardArrowRight)`
    margin-left: 8px;
    font-size: 20px;
    background: #ff33cc;
`;

export const HeroMobileLogo = styled.img`
    display: none;
    
    @media screen and (max-width: 1280px)
    {
        display: flex;
        width: 100%;
        height: 100%;
    }    
`;