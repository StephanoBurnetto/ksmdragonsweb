import React, {useState} from 'react';
import {
  HeroContainer,
  HeroBg,
  VideoBg, HeroMobileLogo
} from './HeroElements';

import BG_VIDEO from '../../assets/video/dragons_VIDEO.mp4';
import BG_MOBILE from '../../assets/logos/mob_log_2.jpg';

const HeroSection = () => {
  return (
  <HeroContainer id="home">
    <HeroBg id="bg">
      <HeroMobileLogo src={BG_MOBILE}/>
      <VideoBg autoPlay muted loop src={BG_VIDEO} type='video/mp4'/>
    </HeroBg>
  </HeroContainer>
  );
};

export default HeroSection;
